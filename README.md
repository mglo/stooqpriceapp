# Project Title

StooqPrice Application

## Getting Started

Android applicaiton which will help you check your current stoq price from stooq.com web page. Project is under development.

### Prerequisites

Android Studio 3.1.2

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Mateusz Głowa** - *Initial work* - [mglo](https://gitlab.com/mglo)



