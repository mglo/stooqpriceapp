package com.app.mglo.stooqprice.scrapData;

import android.os.AsyncTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class DownloadStoqPrice {

    public static double scrapCurrentPrice(final String symbol) throws IOException {
        final String[] price = new String[1];

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                String url = "https://stooq.pl/q/?s=";
                String completeURL = url + symbol;
                Document doc = null;

                try {
                    doc = Jsoup.connect(completeURL).get();
                } catch (IOException e) {
                    System.out.println("problem ze sciaganiem danych");
                }

                price[0] = doc.getElementById("aq_"+symbol+"_c2|3").text();
            }
        });

        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return Double.parseDouble(price[0]);
    }

}