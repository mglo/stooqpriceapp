package com.app.mglo.stooqprice.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.app.mglo.stooqprice.R;
import com.app.mglo.stooqprice.sharedData.SaveSharedPreferences;
import com.app.mglo.stooqprice.sharedData.Stock;
import com.app.mglo.stooqprice.sharedData.StockListAdapter;

import java.util.ArrayList;
import java.util.List;

public class ItemManagerActivity extends AppCompatActivity {

    List<Stock> stocksList;
    ListView stockListView;
    Button addStockButton;
    Button removeStockButton;
    Button saveState;
    EditText symbolText;
    EditText amountText;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_item_layout);

        stockListView = findViewById(R.id.stoqElementsListID);

        addStockButton = findViewById(R.id.addStockButtonID);
        removeStockButton = findViewById(R.id.removeStockButtonID);
        saveState = findViewById(R.id.saveButtonID);

        symbolText = findViewById(R.id.symbolEditTextID);
        amountText = findViewById(R.id.amountEditTextID);


        stocksList = new ArrayList<>();

        final StockListAdapter stockListAdapter = new StockListAdapter(this,R.layout.add_custom_layout, stocksList);
        //CustomInnerAdapter CustomInnerAdapter = new CustomInnerAdapter();

        stockListView.setAdapter(stockListAdapter);

        stockListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object itemobject = adapterView.getAdapter().getItem(index);

                Stock itemDto = (Stock) itemobject;

                CheckBox itemCheckbox = view.findViewById(R.id.checkBoxID);

                if(itemDto.isChecked())
                {
                    itemCheckbox.setChecked(false);
                    itemDto.setChecked(false);
                }else
                {
                    itemCheckbox.setChecked(true);
                    itemDto.setChecked(true);
                }
            }
        });

        addStockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String symbol = String.valueOf(symbolText.getText());
               int amount = Integer.valueOf(String.valueOf(amountText.getText()));

               if(symbol != null || amount != 0){
                   addNewStock(new Stock(symbol, amount));
                   stockListAdapter.notifyDataSetChanged();
               }
            }
        });


        removeStockButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO searching which stock is marked and removing it from stock list
                //TODO refreshing view in the end
                int size = stocksList.size();

                for (int i = 0; i < size; i++) {
                    Stock dto = stocksList.get(i);

                    if(dto.isChecked()){
                        removeStock(stocksList.get(i));
                        i--; //TODO sprytne cofanie sie o jeden po usunieciu - moze lepiej cos pokombinowac
                        size = stocksList.size();
                    }
                }

                stockListAdapter.notifyDataSetChanged();

            }
        });

        saveState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO saving current stockList in to SharedPrefferences
                String stoqNameToSave = "";
                String stoqAmountToSave = "";

                int size = stocksList.size();

                for (int i = 0; i < size; i++) {
                    Stock dto = stocksList.get(i);

                    if(dto.isChecked()){
                        stoqNameToSave += dto.getSymbol() + ";";
                        stoqAmountToSave += dto.getAmount() + ";";
                    }
                }
                SaveSharedPreferences.setClientStoqs(getApplicationContext(), stoqNameToSave, stoqAmountToSave);
        }
        });
    }


    private void addNewStock(Stock stock){
        stocksList.add(stock);
    }

    private void removeStock(Stock stock){
        stocksList.remove(stock);
    }


}
