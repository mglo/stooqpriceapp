package com.app.mglo.stooqprice.sharedData;

public final class PreferencesUtility {

    public static final String STOQ_NAME = "stoq_name";
    public static final String STOQ_AMMOUNT = "stoq_amount";
    public static final String STOQ_SET = "stoq_set";
}
