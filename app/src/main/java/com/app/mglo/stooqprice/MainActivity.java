package com.app.mglo.stooqprice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.app.mglo.stooqprice.activities.ItemManagerActivity;
import com.app.mglo.stooqprice.scrapData.DownloadStoqPrice;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    ListView mListView;
    Button refreshButton;
    Button addStooqsButton;
    String[] akcjeName = {"if4"};
    double[] moneyEarned = {1.0};
    double[] currentPrice = {1.0};
    int stoqAmmount = 1191;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addStooqsButton = findViewById(R.id.addButtonID);
        refreshButton = findViewById(R.id.refreshButtonID);

        mListView = findViewById(R.id.listViewItem);
        CustomAdapter customAdapter = new CustomAdapter();

        try {
            refreshView();
        } catch (IOException e) {
            e.printStackTrace();
        }

        moneyEarned[0] = moneyCalc(currentPrice[0], stoqAmmount);
        mListView.setAdapter(customAdapter);

        //TODO implement observer design pattern ?? --later
        //TODO colorfull data
        //TODO add draft table in add Layout

        addStooqsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ItemManagerActivity.class));
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    class CustomAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return akcjeName.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = getLayoutInflater().inflate(R.layout.customlayout, null);

            TextView mTextView = view.findViewById(R.id.companyNameID);
            TextView mMoneyearned = view.findViewById(R.id.moneyCounterID);
            TextView mcurrentPrice = view.findViewById(R.id.priceID);

            mTextView.setText(akcjeName[position]);
            mMoneyearned.setText(String.valueOf(moneyEarned[position]));
            mcurrentPrice.setText(String.valueOf(currentPrice[position]));

            return view;
        }
    }

    public void refreshView() throws IOException {
        currentPrice[0] = DownloadStoqPrice.scrapCurrentPrice("if4");
    }

    public double moneyCalc(double price, int stoqAmmount){
        return price * stoqAmmount;
    }
}
