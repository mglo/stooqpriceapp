package com.app.mglo.stooqprice.sharedData;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Objects;

public class Stock {

    private String symbol;
    private int amount;
    private boolean checkedState = false;

    public Stock(String symbol, int amount) {
        this.symbol = symbol;
        this.amount = amount;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isChecked() {
        return checkedState;
    }

    public void setChecked(boolean checkedState) {
        this.checkedState = checkedState;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stock stock = (Stock) o;
        return amount == stock.amount &&
                Objects.equals(symbol, stock.symbol);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(symbol, amount);
    }
}
