package com.app.mglo.stooqprice.sharedData;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.app.mglo.stooqprice.sharedData.PreferencesUtility.STOQ_AMMOUNT;
import static com.app.mglo.stooqprice.sharedData.PreferencesUtility.STOQ_NAME;

public class SaveSharedPreferences {

    static SharedPreferences getPref(Context ctx){
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setClientStoqs(Context context, String stoqName, String stoqAmount){
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString(STOQ_NAME, stoqName); //xxx;xxx;xxx;xx
        editor.putString(STOQ_AMMOUNT, stoqAmount); //xxx;xxx;xxx;xxx
        editor.apply();
    }

    public static String getStoqName(Context ctx){
        return getPref(ctx).getString(STOQ_NAME, "none");
    }

    public static String getStoqAmount(Context ctx){
        return getPref(ctx).getString(STOQ_AMMOUNT, "none");
    }
}
