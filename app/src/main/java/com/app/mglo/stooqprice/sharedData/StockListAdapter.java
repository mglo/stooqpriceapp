package com.app.mglo.stooqprice.sharedData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mglo.stooqprice.R;

import org.w3c.dom.Text;

import java.util.List;

public class StockListAdapter extends BaseAdapter {

    private static final String TAG = "StockListAdapter";

    private Context context;
    int resource;
    private List<Stock> stockItemDtoList = null;


    public StockListAdapter(@NonNull Context context, int resource, @NonNull List<Stock> objects) {
        this.context = context;
        this.resource = resource;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if(stockItemDtoList!=null)
        {
            ret = stockItemDtoList.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int itemIndex) {
        Object ret = null;
        if(stockItemDtoList!=null) {
            ret = stockItemDtoList.get(itemIndex);
        }
        return ret;
    }

    @Override
    public long getItemId(int itemIndex) {
        return itemIndex;
    }

    @NonNull
    @Override
    public View getView(int itemPosition, @Nullable View convertView, @NonNull ViewGroup parent) {

        ListViewItemViewHolder viewHolder = null;

        if(convertView != null){
            viewHolder = (ListViewItemViewHolder) convertView.getTag();
        }else{
            convertView = View.inflate(context,R.layout.add_new_item_layout, null);

            CheckBox listItemCheckBox = convertView.findViewById(R.id.checkBoxID);
            TextView stockName = convertView.findViewById(R.id.TVsymbolID);
            TextView amount = convertView.findViewById(R.id.TVamountID);

            viewHolder = new ListViewItemViewHolder(convertView);

            viewHolder.setItemCheckbox(listItemCheckBox);
            viewHolder.setItemTextView(stockName);
            viewHolder.setItemTextView2(amount);

            convertView.setTag(viewHolder);
        }

        Stock listStocksDTO = stockItemDtoList.get(itemPosition);
        viewHolder.getItemCheckbox().setChecked(listStocksDTO.isChecked());
        viewHolder.getItemTextView().setText(listStocksDTO.getSymbol());
        viewHolder.getItemTextView2().setText(listStocksDTO.getAmount());

        return convertView;
    }

}
